function fit_and_predict_covid(n, training, delay)
    if nargin<3
        delay = 1;
    end
    [rows,cols] = size(training);
    days = 1:n;
    actual = 1:rows;
    
    if rows>delay
        past = 1:(rows-delay);
        training_past = training(past);
        test_past = training(rows-delay+1:rows);
        %disp(test_past);
        
        %logit fitting at n days before
        [~,p_past,~,~] = fit_logistic(past,training_past');
        Qinf_past = p_past(2);
        thalf_past = p_past(1);
        alpha_past = p_past(3);
        %logit prediction at n days before
        Ypredicted_past = Qinf_past./(1 + exp(-alpha_past*(days-thalf_past)));
        %logit prediction RMSE and MAPE
        logit_test = Ypredicted_past(rows-delay+1:rows)';
        RMSElogit = sqrt(mean((test_past - logit_test).^2));
        MAPElogit = mean(abs((test_past - logit_test)./test_past));
        
        %exp fitting at n days before
        f = fit(past', training_past,'exp1');
        %exp prediction at n days before
        Yexp = f(days);
        %exp prediction RMSE and MAPE
        exp_test = Yexp(rows-delay+1:rows);
        RMSEexp = sqrt(mean((test_past - exp_test).^2));
        MAPEexp = mean(abs((test_past - exp_test)./test_past));
        
        %simply prints out the root mean squared errors
        disp(strcat('RMSE against exponential:',num2str(RMSEexp)));
        disp(strcat('RMSE against logit:',num2str(RMSElogit)));
        disp(strcat('MAPE against exponential:',num2str(MAPEexp)));
        disp(strcat('MAPE against logit:',num2str(MAPElogit)));
    end    
    
    %actual model fitting
    [~,p,~,~] = fit_logistic(actual,training');
    Qinf = p(2);
    thalf = p(1);
    alpha = p(3);
    %actual model prediction step at n days
    days = 1:n;
    Ypredicted = Qinf./(1 + exp(-alpha*(days-thalf)));
    
    %plot the predictions, the data and the inflection point as vertical line
    hold on;
    plot(actual, training', '-xb', 'LineWidth', 3);
    plot(days, Ypredicted, '-o', 'LineStyle', '-');
    if exist('Ypredicted_past')
        plot(days, Ypredicted_past, '-g', 'LineStyle', '-');
        plot(days, Yexp, '-r', 'LineStyle', '-');
        %plot(days, Ylin, '-m', 'LineStyle', '-');
    end
    %draw inflection point
    line([thalf thalf], [0 Qinf],'LineStyle','--');
    %draw end of training set
    line([rows-delay rows-delay], [0, Qinf],'LineStyle','-');
    xlabel('i-esimo giorno (a partire dal 24 Febbraio 2020)');
    ylabel('persone');
    ylim([0 Qinf])
    legend('dato reale prot. civ.', 'prev. logit oggi', 'prev. logit precedente', 'prev. exp precedente','Location','southeast');
    hold off;